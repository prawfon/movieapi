package projectkotlin.demo.entity.Dto

data class SubtitleDto(var subtitle: String? = null)