package projectkotlin.demo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

interface User {
    var userName: String?
    var password: String?
    var email: String?
    var firstName: String?
    var lastName: String?

}
