package projectkotlin.demo.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import projectkotlin.demo.dao.SubtitleDao
import projectkotlin.demo.entity.Subtitle

@Service
class SubtitleServiceImpl:SubtitleService{
    @Autowired
    lateinit var subtitleDao: SubtitleDao

    override fun getSubtitle(): List<Subtitle> {
        return subtitleDao.getSubtitle()
    }
}
