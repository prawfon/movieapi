package projectkotlin.demo.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import projectkotlin.demo.dao.SelectedSeatDao
import projectkotlin.demo.entity.SelectedSeat

@Service
class SelectedSeatServiceImpl:SelectedSeatService{
//    override fun getSelectedSeatByHorizontal(horizontal: Int): List<SelectedSeat> {
//        return selectedSeatDao.getSelectedSeatByHorizontal(horizontal)
//    }
//
//    override fun getSelectedSeatByVertical(vertical: Int): List<SelectedSeat> {
//        return selectedSeatDao.getSelectedSeatByVertical(vertical)
//    }

    @Autowired
    lateinit var selectedSeatDao: SelectedSeatDao
    override fun getSelectedSeats(): List<SelectedSeat> {
        return selectedSeatDao.getSelectedSeats()
    }
}