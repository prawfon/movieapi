package projectkotlin.demo.service

import projectkotlin.demo.entity.Subtitle

interface SubtitleService{
    fun getSubtitle(): List<Subtitle>
}
