package projectkotlin.demo.service

import projectkotlin.demo.entity.Cinema


interface CinemaService{
    fun getCinema():List<Cinema>
    fun getCinemaByName(name: String): List<Cinema>
}