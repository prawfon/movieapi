package projectkotlin.demo.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.demo.entity.Movies

interface MovieRepository: CrudRepository<Movies, Long> {
    fun findBymovieNameContainingIgnoreCase(name: String): List<Movies>
}