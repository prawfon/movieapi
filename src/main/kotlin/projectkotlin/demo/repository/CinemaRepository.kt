package projectkotlin.demo.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.demo.entity.Cinema

interface CinemaRepository: CrudRepository<Cinema, Long> {
    fun findBynameContainingIgnoreCase(name: String): List<Cinema>
}