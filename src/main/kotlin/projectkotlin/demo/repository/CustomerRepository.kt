package projectkotlin.demo.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.demo.entity.Customer


interface CustomerRepository : CrudRepository<Customer, Long> {

    fun findByuserNameContainingIgnoreCase(name: String): List<Customer>
    fun findByfirstNameContainingIgnoreCase(name: String): List<Customer>
    fun findBylastNameContainingIgnoreCase(name: String): List<Customer>
}