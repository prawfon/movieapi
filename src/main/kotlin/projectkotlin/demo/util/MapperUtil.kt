package projectkotlin.demo.util
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers
import projectkotlin.demo.dao.ShowtimeDao
import projectkotlin.demo.entity.*
import projectkotlin.demo.entity.Dto.*


@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }
    fun mapMovieDto(movie: Movies): MovieDto
    fun mapMovieDto(movies: List<Movies>): List<MovieDto>

    fun mapCustomerDto(customer: Customer): CustomerDto
    fun mapCustomerDto(customers: List<Customer>): List<Customer>
    @InheritInverseConfiguration
    fun mapCustomerDto(customer:CustomerDto): Customer

    fun mapSoundtrackDto(soundtrack: Soundtrack): SoundtrackDto
    fun mapSoundtrackDto(soundtracks: List<Soundtrack>): List<Soundtrack>

    fun mapSubtitleDto(subtitle: Subtitle): SubtitleDto
    fun mapSubtitleDto(subtitles: List<Subtitle>): List<Subtitle>

    fun mapCinemaDto(cinema: Cinema): CinemaDto
    fun mapCinemaDto(cinemas: List<Cinema>): List<Cinema>

    fun mapSeatDto(seat: Seats): SeatsDto
    fun mapSeatDto(seats: List<Seats>): List<Seats>


//    fun mapShowtimeDto(showtime: Showtime): ShowtimeDto
//    fun mapShowtimeDto(showtimes: List<Showtime>): List<Showtime>
    fun mapShowtimeDto(showtime: Showtime?): ShowtimeDto?
    fun mapShowtimeDto(showtimes: List<Showtime>): List<ShowtimeDto>
    @InheritInverseConfiguration
    fun mapShowtimeDto(showtimeDto: ShowtimeDto): Showtime

    fun mapSelectedSeatDto(selectedSeat: SelectedSeat): SelectedSeatDto
    fun mapSelectedSeatDto(selectedSeat: List<SelectedSeat>): List<SelectedSeat>

}
