package projectkotlin.demo.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import projectkotlin.demo.service.MoviesService
import projectkotlin.demo.util.MapperUtil


@RestController
class MoviesController{
   @Autowired
    lateinit var moviesService: MoviesService
    @GetMapping("/Movies")
    fun getAllMovies():ResponseEntity<Any>{
        var movies = MapperUtil.INSTANCE.mapMovieDto(moviesService.getMovies())
        return ResponseEntity.ok(movies)
    }
    @GetMapping("/movie/movieName")
    fun getMovieBymovieName(@RequestParam("movieName") name: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapMovieDto(moviesService.getMovieByMovieName(name))
        output?.let { return ResponseEntity.ok(output) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }


}


//@RestController
//class MoviesController{
//    @Autowired
//    lateinit var moviesService: MoviesService
//    @GetMapping("/Movies")
//    fun getAllMovies():ResponseEntity<Any>{
//        val movie = Movies("Dumbo",
//                "A young elephant, whose oversized ears enable him to fly, helps save a struggling circus. But when the circus plans a new venture, Dumbo and his friends discover dark secrets beneath its shiny veneer.",
//                "https://lh3.googleusercontent.com/FcKcdorsSc0A1gcPJ6Vgwjd1oVU0wgZP6m6rvOEj-INliNOeILq-ND3pR41N2RZCCP5scslco2shHqaVIq7c=w260")
//        return  ResponseEntity.ok(movie);
//    }
//}