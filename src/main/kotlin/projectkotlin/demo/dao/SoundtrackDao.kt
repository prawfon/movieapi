package projectkotlin.demo.dao

import projectkotlin.demo.entity.Soundtrack

interface SoundtrackDao{
    fun getSoundtrack():List<Soundtrack>
}