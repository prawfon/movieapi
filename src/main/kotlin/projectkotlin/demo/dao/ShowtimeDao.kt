package projectkotlin.demo.dao

import projectkotlin.demo.entity.Showtime


interface ShowtimeDao{
    fun getShowtimes(): List<Showtime>
    fun getShowtimeByCinemaname(name: String): List<Showtime>
    fun getShowtimeByMoviename(name: String): List<Showtime>
    fun findById(showTimeId: Long?): Showtime
}