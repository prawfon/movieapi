package projectkotlin.demo.dao

import projectkotlin.demo.entity.SelectedSeat

interface SelectedSeatDao{

    fun getSelectedSeats(): List<SelectedSeat>
//    fun getSelectedSeatByVertical(vertical: String): List<SelectedSeat>
//    fun getSelectedSeatByHorizontal(horizontal: String): List<SelectedSeat>

}