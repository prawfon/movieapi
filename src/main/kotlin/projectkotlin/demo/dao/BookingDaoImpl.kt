package projectkotlin.demo.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import projectkotlin.demo.entity.Booking
import projectkotlin.demo.repository.BookingRepository

@Profile("db")
@Repository
class BookingDaoDBImpl:BookingDao {
    @Autowired
    lateinit var bookingRepository: BookingRepository
}