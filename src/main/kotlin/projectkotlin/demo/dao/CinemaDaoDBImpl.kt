package projectkotlin.demo.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import projectkotlin.demo.entity.Cinema
import projectkotlin.demo.repository.CinemaRepository

@Profile("db")
@Repository
class CinemaDaoDBImpl:CinemaDao{


    override fun getCinemaByName(name: String): List<Cinema> {
        return cinemaRepository.findBynameContainingIgnoreCase(name)
    }

    override fun getAllCinema(): List<Cinema> {
        return cinemaRepository.findAll().filterIsInstance(Cinema::class.java)
    }
    @Autowired
    lateinit var cinemaRepository: CinemaRepository
}
