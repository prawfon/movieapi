package projectkotlin.demo.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import projectkotlin.demo.entity.Soundtrack
import projectkotlin.demo.repository.SoundtrackRespository

@Profile("db")
@Repository
class SoundtrackDaoDBImpl:SoundtrackDao{

    override fun getSoundtrack(): List<Soundtrack> {
        return soundtrackRespository.findAll().filterIsInstance(Soundtrack::class.java)
    }
    @Autowired
    lateinit var soundtrackRespository: SoundtrackRespository
}